#include <LGPS.h>

gpsSentenceInfoStruct info;

char buff[256];
double raw_latitude;
double raw_longitude;
int latitude;
int longitude;
char latitude_buff[20];
char longitude_buff[20];
String gps_data_lat_lon;
String user_account = "0024105";

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    raw_latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    raw_longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", raw_latitude, raw_longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // put your setup code here, to run once:
  //Serial.begin(115200);
  Serial1.begin(9600);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
  Serial.println("=======INITIAL========");
  user_account_uplink(user_account);
  Serial.println("=======INITIAL END========");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(5000);
  Serial1.println("ATZ");
  delay(1000);
  Serial.println();
  Serial.println("=======RUN PROGRAM========");
  gps_raw_data_to_int(raw_latitude,raw_longitude);
  split_float_and_uplink(latitude,longitude);
  Serial.println("=======END PROGRAM=========");
  Serial.println();
}

//Uplink the account data by LoRa module
void user_account_uplink(String account){
  Serial1.println("ATZ");
  delay(1000);
  user_account="c"+user_account;
  Serial.println();
  Serial.println("Account："+user_account);
  Serial1.println("AT+DTX=8,"+user_account);
  Serial.println();
  Serial.println("Transmit the Account data...");
  Serial.println();
}

//GPS the raw data transform GPS Integer
void gps_raw_data_to_int(double lat,double lon){
  Serial.println();
  sprintf(latitude_buff,"%f\n",lat*10000);
  sprintf(longitude_buff,"%f\n",lon*10000);
  Serial.print("LATITUDE：");
  Serial.println(String(latitude_buff).toInt());
  Serial.println();
  Serial.print("LONGITUDE：");
  Serial.println(String(longitude_buff).toInt());
  latitude=String(latitude_buff).toInt();
  longitude=String(longitude_buff).toInt();
}

//Uplink the GPS data by LoRa module
void split_float_and_uplink(long lat,long lon){

  long lat_b = lat%1000000;
  long lat_a=(lat-lat_b)/1000000;
  double lat_b_tran=(double)lat_b/600000*1000000;

  long lon_b = lon%1000000;
  long lon_a=(lon-lon_b)/1000000;
  double lon_b_tran=(double)lon_b/600000*1000000;

  gps_data_lat_lon=String(lat_a)+"a"+String((int)lat_b_tran)+"b"+String(lon_a)+"a"+String((int)lon_b_tran);
  Serial.println();
  Serial.print("GPS DATA：");
  Serial.println(gps_data_lat_lon);
  Serial1.println("AT+DTX=20,"+gps_data_lat_lon);
  Serial.println();
  Serial.println("Transmit the GPS data...");
  Serial.println();
}

